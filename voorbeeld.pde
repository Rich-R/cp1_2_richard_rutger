import ddf.minim.*;
import ddf.minim.analysis.*;

Minim minim;
AudioPlayer song;
BeatDetect beat;

void setup() {
  size(1920, 1080);
  minim = new Minim(this);
  song = minim.loadFile("Claptone - No Eyes (feat. Jaw).mp3", 2048);
  song.play();
  beat = new BeatDetect();
  frameRate(30);
}

void draw() {
  float hue = 0.65;
noStroke();
  background(0);
  float ch;
  float cw;
  beat.detect(song.mix);
  if ( beat.isOnset() ) {
    loop();
  ch = random(height / 6.7 + 50,height / 6.7 + 75);
  cw = random((width / 2) + 50 ,(width / 2) + 200);
  hue = random(0.01,0.5);
}
else {
  ch = height / 6.7;
  cw = width / 2;
}
  float x = width / 2;
  float y = height / 2;

  float x1 = x - cw / 2;
  float x2 = x + cw / 2;
  float y1 = y - ch - cw / 4;
  float y2 = y - ch;
  float y3 = y - ch + cw /4;
  float y4 = y + cw / 4;
  colorMode(HSB, 1, 1, 1, 1);
  
  fill(hue, .7, .7);
  quad(x1, y, x1, y2, x, y3, x, y4);
  fill(hue, .7, .6);
  quad(x2, y, x2, y2, x, y3, x, y4);
  fill(hue, .7, .8);
  quad(x, y1, x2, y2, x, y3, x1, y2);
  delay(45);
}