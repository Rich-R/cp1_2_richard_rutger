import ddf.minim.*;
import ddf.minim.analysis.*;
Minim minim;
AudioPlayer song;
BeatDetect beat;

 void setup() {
  size(1000, 1000);
  background(0);
  noStroke();
  colorMode(HSB, 1, 1, 1, 1);
  minim = new Minim(this);
  song = minim.loadFile("Claptone - No Eyes (feat. Jaw).mp3", 2048);
  song.play();
  beat = new BeatDetect();
  frameRate(30);
}

void draw() {
noStroke();
  background(0);
   for (int i = 0; i < 50; i++) {
   float hue = random(0.4 ,.9);
   float x = random(100, width - 100);
   float y = random(100, height - 100);
   float diameter = random(10, 150);
   float brness = random(0.5,0.6);
    beat.detect(song.mix);
     if ( beat.isOnset() ) {
       loop();
    diameter = random(500, 1000);
    if (diameter > 750){
    hue = random(0.01,0.12);
    }
    else {hue = random(0.15,0.25);
    }
    x = random( width/2 - 200, width/2 + 200);
    y = random( height/2 - 200, height/2 + 200);
     brness = random(0.7,0.8);
  }
   fill(hue, .75, 1, brness);
   ellipse(x, y, diameter, diameter);
}
delay(100); //hierdoor gaat de animatie een stuk minder snel, wat in dit geval de beat duidelijker maakt
}