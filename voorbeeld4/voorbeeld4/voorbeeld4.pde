import ddf.minim.*;
import ddf.minim.analysis.*;

Minim minim;
AudioPlayer song;
BeatDetect beat;

int[] xPos = new int[220];
int[] yPos = new int[220];
int[] Ahue = new int[220];

int[] cubeSize = new int[220];
int[] currentCubeSize = new int[220];


void setup() {
  size(1920, 1080);
  background(0);
  minim = new Minim(this);
  song = minim.loadFile("Claptone - Wrong - Exploited.mp3", 2048);
  song.play();
  beat = new BeatDetect();


  for (int i = 0; i < 220; i++) {
    int size = round(random(width));
    int size2 = round(random(height));
    xPos[i] = size;
    yPos[i] = size2;
  }
  for (int i = 0; i < 220; i++) {
    int size = round(random(50, 250));
    cubeSize[i] = size;
    currentCubeSize[i] = size;
  }
      for (int i = 0; i < 220; i++) {
      int hue;
      if (xPos[i] < width / 4) {
        hue = round(random(75, 99));
      } else if (xPos[i] > width/4 && xPos[i] < width/2) {
        hue = round(random(50, 85));
      } else if (xPos[i] > width/2 && xPos[i] < (width * 3)/4) {
        hue = round(random(25, 60));
      } else {
        hue = round(random(0, 35));
      }
      Ahue[i] = hue;
    }
}

void draw() {
  background(0);
    for (int i = 0; i < 220; i++) {
      cube(xPos[i], yPos[i], 100, currentCubeSize[i], Ahue[i]);
      currentCubeSize[i] += 1;
    }
    beat.detect(song.mix);
    if ( beat.isOnset() ) {
          for (int i = 0; i < 220; i++) {
    currentCubeSize[i] = round(random(cubeSize[i]));
          }
  }
  }

  void cube(float x, float y, float cw, float ch, float hue) {
    float x1 = x - cw / 2;
    float x2 = x + cw / 2;
    float y1 = y - ch - cw / 4;
    float y2 = y - ch;
    float y3 = y - ch + cw /4;
    float y4 = y + cw / 4;
    colorMode(HSB, 100, 100, 100, 100);
    noStroke();
    fill(hue, 70, 70);
    quad(x1, y, x1, y2, x, y3, x, y4);
    fill(hue, 70, 60);
    quad(x2, y, x2, y2, x, y3, x, y4);
    fill(hue, 70, 80);
    quad(x, y1, x2, y2, x, y3, x1, y2);
  }